import httplib2
import pandas as pd
import df2gspread as d2g
import requests
import json
from datetime import date
import configparser

from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

config = configparser.ConfigParser()
config.read("/etc/bot/andrew/stocks/stocks", encoding="utf8")
stocks_list = []
for key in config['stocks']:
    stocks_list.append(key)

# diff = list(set(stocks_list).difference(id))
# print(config['stocks']['159402'])

df_main = pd.DataFrame({'Артикул':[], 'Идентификатор':[], 'Склад':[], 'Остаток':[], 'Продажи':[]})

scopes = ['https://www.googleapis.com/auth/spreadsheets']
creds_service = ServiceAccountCredentials.from_json_keyfile_name(
    '/etc/bot/big-elysium-364512-847480f2e88d.json', scopes).authorize(httplib2.Http())
my_table = build('sheets', 'v4', http=creds_service)
resp = my_table.spreadsheets().values().batchGet(
    spreadsheetId='1uS_Tt_I_UKj-AxAkSxsrGXJTLwwxJCBZgRpsxRBmnD8', ranges="Лист3").execute()
print(resp['valueRanges'][0]['values'])
# df = pd.DataFrame(resp['valueRanges'][0]['values'])
# df.columns = df.loc[0, :]
# df.drop(labels=0, axis=0, inplace=True)

# for i in resp['valueRanges'][0]['values'][1:]:
#     print(i[0], i[1])
for product in resp['valueRanges'][0]['values'][1:]:
    art = product[0]
    ident = product[1]
    url = "https://card.wb.ru/cards/detail?spp=0&regions=80,64,38,4,83,33,68,70,69,30,86,75,40,1,22,66,31,48,71&" \
          "pricemarginCoeff=1.0&reg=0&appType=1&emp=0&locale=ru&lang=ru&curr=rub&couponsGeo=" \
          "12,3,18,15,21&dest=-1257786&nm={art}".format(art=art)  # 5163995;6170054;31211424"

    res = requests.get(url, headers={
        "accept": "*/*",
        "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
        "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "cross-site"
    })

    for stock in stocks_list:
        line = [art, ident, config['stocks'][stock], 0, 0]
        df_main.loc[len(df_main.index)] = line

    res_json = json.loads(res.text)
    print(res_json['data']['products'][0]['sizes'][0]['stocks'])
    real_stocks = []
    for stock in res_json['data']['products'][0]['sizes'][0]['stocks']:
        stock_name = config['stocks'][str(stock['wh'])]
        df_main.loc[(df_main['Артикул'] == art) & (df_main['Склад'] == stock_name), ['Остаток']] = stock['qty']


# print(df_main)

body = {
        'data': [
            {
                'range' : 'Склады',
                'majorDimension': 'COLUMNS',
                'values': [df_main.columns.values.tolist()]
            },
            {
                'range' : 'Склады',
                'majorDimension': 'ROWS',
                'values': df_main.values.tolist()
            }
        ]
    }

request = my_table.spreadsheets().values().update(spreadsheetId='1uS_Tt_I_UKj-AxAkSxsrGXJTLwwxJCBZgRpsxRBmnD8',
                                                    valueInputOption='USER_ENTERED',
                                                    range='Склады',
                                                    body={'values' : [df_main.columns.tolist()] + df_main.values.tolist()}).execute()
