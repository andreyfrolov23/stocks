import time
import httplib2
import pandas as pd
# import df2gspread as d2g
import requests
import json
import configparser
from datetime import date, datetime

from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

today = datetime.now().date()
# today = '2023-07-05'

config = configparser.ConfigParser()
config.read("/etc/bot/andrew/stocks/stocks")

sheet_vitaliy = config['sheets']['vitaliy']
scopes = ['https://www.googleapis.com/auth/spreadsheets']
creds_service = ServiceAccountCredentials.from_json_keyfile_name\
    ('/etc/bot/big-elysium-364512-847480f2e88d.json', scopes).authorize(httplib2.Http())
my_table = build('sheets', 'v4', http=creds_service)


def stock_qty(my_list):
    stock = config['stocks'][str(my_list['wh'])]
    # qty = my_list['qty']
    # print(stock, my_list['qty'])
    return stock, my_list['qty']


def new_day(date):
    table = my_table.spreadsheets().values().batchGet(spreadsheetId=sheet_vitaliy, ranges="Склады с размерами").execute()
    df_table = pd.DataFrame(table['valueRanges'][0]['values'])
    df_table.columns = df_table.loc[0, :]
    df_table.drop(labels=0, axis=0, inplace=True)
    new_line = df_table['Продажи за день'].tolist()
    # today_date = date.today()
    df_table[str(date)] = new_line
    df_table['Продажи за день'] = 0
    body = {
        'data': [
            {
                'range': 'Склады с размерами',
                'majorDimension': 'COLUMNS',
                'values': [df_table.columns.values.tolist()]
            },
            {
                'range': 'Склады с размерами',
                'majorDimension': 'ROWS',
                'values': df_table.values.tolist()
            }
        ]
    }

    request = my_table.spreadsheets().values().update(spreadsheetId=sheet_vitaliy,
                                                      valueInputOption='USER_ENTERED',
                                                      range='Склады с размерами',
                                                      body={
                                                          'values': [
                                                                        df_table.columns.tolist()] + df_table.values.tolist()}).execute()


while True:
    table_sizes = my_table.spreadsheets().values().batchGet(spreadsheetId=sheet_vitaliy, ranges="Склады с размерами!A1:F").execute()
    df_table_sizes = pd.DataFrame(table_sizes['valueRanges'][0]['values'])
    df_table_sizes.columns = df_table_sizes.loc[0, :]
    df_table_sizes.drop(labels=0, axis=0, inplace=True)

    #print(df_table_sizes)
    arts = []
    for art in df_table_sizes['Артикул']:
        if art not in arts:
            arts.append(art)
    for art in arts:
        url = "https://card.wb.ru/cards/detail?spp=0&regions=80,64,38,4,83,33,68,70,69,30,86,75,40,1,22,66,31,48,71&" \
                  "pricemarginCoeff=1.0&reg=0&appType=1&emp=0&locale=ru&lang=ru&curr=rub&couponsGeo=" \
                  "12,3,18,15,21&dest=-1257786&nm={art}".format(art=art)  # 5163995;6170054;31211424"

        res = requests.get(url, headers={
                "accept": "*/*",
                "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
                "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
                "sec-ch-ua-mobile": "?0",
                "sec-ch-ua-platform": "\"Windows\"",
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "cross-site"
        })

        res_json = json.loads(res.text)

        for i_list in res_json['data']['products'][0]['sizes']:
            # print(i_list)
            if i_list['stocks']:
                for i_stock in i_list['stocks']:
                    stock_qty(i_stock)
                    stock, qty = stock_qty(i_stock)
                    #print(i_list['origName'], stock, qty)
                    if df_table_sizes.loc[(df_table_sizes['Артикул'] == art) & (df_table_sizes['Размер'] == i_list['origName'])
                                          & (df_table_sizes['Склад'] == stock)].empty:
                        ident = df_table_sizes.loc[(df_table_sizes['Артикул'] == art), ['Идентификатор']].values[0][0]
                        new_row = {'Артикул': art, 'Идентификатор': ident, 'Размер': i_list['origName'], 'Склад': stock,
                                   'Остаток': qty, 'Продажи за день': 0}
                        df1 = pd.DataFrame([new_row])
                        df_table_sizes = pd.concat([df_table_sizes, df1], ignore_index=True)
                    else:
                        delta = int(df_table_sizes.loc[((df_table_sizes['Артикул'] == art) &
                                                        (df_table_sizes['Размер'] == i_list['origName']) &
                                                        (df_table_sizes['Склад'] == stock)), ['Остаток']].values[0][0]) - int(qty)
                        if delta > 0:
                            day_sales = int(df_table_sizes.loc[((df_table_sizes['Артикул'] == art) &
                                                        (df_table_sizes['Размер'] == i_list['origName']) &
                                                        (df_table_sizes['Склад'] == stock)), ['Продажи за день']].values[0][0]) + delta
                            df_table_sizes.loc[((df_table_sizes['Артикул'] == art) &
                                                (df_table_sizes['Размер'] == i_list['origName']) &
                                                (df_table_sizes['Склад'] == stock)), ['Продажи за день']] = day_sales
                            df_table_sizes.loc[((df_table_sizes['Артикул'] == art) &
                                                (df_table_sizes['Размер'] == i_list['origName']) &
                                                (df_table_sizes['Склад'] == stock)), ['Остаток']] = qty
                        else:
                            df_table_sizes.loc[((df_table_sizes['Артикул'] == art) &
                                                (df_table_sizes['Размер'] == i_list['origName']) &
                                                (df_table_sizes['Склад'] == stock)), ['Остаток']] = qty

    body = {
        'data': [
            {
                'range': 'Склады с размерами!A1:F',
                'majorDimension': 'COLUMNS',
                'values': [df_table_sizes.columns.values.tolist()]
            },
            {
                'range': 'Склады с размерами!A1:F',
                'majorDimension': 'ROWS',
                'values': df_table_sizes.values.tolist()
            }
        ]
    }

    request = my_table.spreadsheets().values().update(spreadsheetId=sheet_vitaliy,
                                                      valueInputOption='USER_ENTERED',
                                                      range='Склады с размерами!A1:F',
                                                      body={'values': [df_table_sizes.columns.tolist()] + df_table_sizes.values.tolist()}).execute()

    if today != datetime.now().date():
        new_day(today)
        today = datetime.now().date()
    time.sleep(190)
