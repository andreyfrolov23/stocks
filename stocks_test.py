import time

import httplib2
import pandas as pd
import df2gspread as d2g
import requests
import json
from datetime import date, datetime
import configparser

from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

today = datetime.now().date()
# today = '2023-07-05'

count = 0

config = configparser.ConfigParser()
config.read("/etc/bot/andrew/stocks/stocks", encoding="utf8")
stocks_list = []
for key in config['stocks']:
    stocks_list.append(key)

sheet = config['sheets']['guys']
sheet_vitaliy = config['sheets']['vitaliy']
sheets_all = [sheet, sheet_vitaliy]
print(sheets_all)

while True:
    # print(today, '!!!!!!!!!!!!!')
    for sheet in sheets_all:
        df_main = pd.DataFrame({'Артикул': [], 'Идентификатор': [], 'Склад': [], 'Остаток': []})  # 'Продажи':[]})
        df_main = df_main.astype({'Артикул': 'str',
                                  'Идентификатор': 'str',
                                  'Склад': 'str',
                                  'Остаток': 'int'})

        scopes = ['https://www.googleapis.com/auth/spreadsheets']
        creds_service = ServiceAccountCredentials.from_json_keyfile_name(
            '/etc/bot/big-elysium-364512-847480f2e88d.json', scopes).authorize(httplib2.Http())
        my_table = build('sheets', 'v4', http=creds_service)
        try:
            resp = my_table.spreadsheets().values().batchGet(
                spreadsheetId=sheet, ranges="Лист3").execute()
        # print(resp['valueRanges'][0]['values'])

            resp2 = my_table.spreadsheets().values().batchGet(
                spreadsheetId=sheet, ranges="Склады").execute()

        except:
            continue
        df_curr = pd.DataFrame(resp2['valueRanges'][0]['values'])
        df_curr.columns = df_curr.loc[0, :]
        df_curr.drop(labels=0, axis=0, inplace=True)

        df_curr = df_curr.astype({'Артикул': 'str',
                                  'Остаток': 'int',
                                  'Идентификатор': 'str',
                                  'Продажи': 'int'
                                  })
        # print(df_curr)

        for product in resp['valueRanges'][0]['values'][1:]:
            art = product[0]
            ident = product[1]
            url = "https://card.wb.ru/cards/detail?spp=0&regions=80,64,38,4,83,33,68,70,69,30,86,75,40,1,22,66,31,48,71&" \
                  "pricemarginCoeff=1.0&reg=0&appType=1&emp=0&locale=ru&lang=ru&curr=rub&couponsGeo=" \
                  "12,3,18,15,21&dest=-1257786&nm={art}".format(art=art)  # 5163995;6170054;31211424"

            res = requests.get(url, headers={
                "accept": "*/*",
                "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
                "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
                "sec-ch-ua-mobile": "?0",
                "sec-ch-ua-platform": "\"Windows\"",
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "cross-site"
            })
            # print(res)

            for stock in stocks_list:
                line = [str(art), str(ident), str(config['stocks'][stock]), 0]
                df_main.loc[len(df_main.index)] = line

            res_json = json.loads(res.text)
            # print(res_json['data']['products'][0]['sizes'][0]['stocks'])
            real_stocks = []
            print(art)
            try:
                for stock in res_json['data']['products'][0]['sizes'][0]['stocks']:
                    try:
                        stock_name = config['stocks'][str(stock['wh'])]
                        df_main.loc[(df_main['Артикул'] == art) & (df_main['Склад'] == stock_name), ['Остаток']] = int(stock['qty'])
                    except:
                        df_main.loc[len(df_main.index)] = [str(art), str(ident), str(stock['wh']), int(stock['qty'])]
            except:
                print('Not found ', art)

        # print(df_main)

        result = df_curr.merge(df_main, on=['Артикул', 'Склад'], how='outer')
        result['Остаток_x'] = result['Остаток_x'].fillna(0)
        result['Остаток_y'] = result['Остаток_y'].fillna(0)
        result['Продажи'] = result['Продажи'].fillna(0)
        result = result.astype({'Артикул': 'str',
                                'Остаток_x': 'int',
                                'Остаток_y': 'int',
                                'Идентификатор_x': 'str',
                                'Идентификатор_y': 'str',
                                'Продажи': 'int'
                                })

        result = result.fillna(0)

        new_line = []
        if today == datetime.now().date():
            for line_index in range(len(result['Остаток_x'])):
                if (int(result['Остаток_x'].tolist()[line_index]) - int(result['Остаток_y'].tolist()[line_index])) in range(1, 50):
                    new_line.append(
                        int(result['Остаток_x'].tolist()[line_index]) - int(result['Остаток_y'].tolist()[line_index]) +
                        int(result['Продажи'].tolist()[line_index]))
                else:
                    new_line.append(int(result['Продажи'].tolist()[line_index]))
        else:
            count += 1
            # print(today, datetime.now().date())
            result.insert(loc=5, column='{date}'.format(date=today), value=result['Продажи'].tolist())
            # result[today] = result['Продажи'].tolist()
            if count == 2:
                today = datetime.now().date()
                count = 0
            for line_index in range(len(result['Остаток_x'])):
                if (int(result['Остаток_x'].tolist()[line_index]) - int(result['Остаток_y'].tolist()[line_index])) in range(1, 50):
                    new_line.append(
                        int(result['Остаток_x'].tolist()[line_index]) - int(result['Остаток_y'].tolist()[line_index]))
                else:
                    new_line.append(0)

        result['Продажи'] = new_line
        result['Идентификатор_x'] = result['Идентификатор_y']
        result = result.rename(columns={'Идентификатор_x': 'Идентификатор'})
        result['Остаток_x'] = result['Остаток_y']
        result = result.rename(columns={'Остаток_x': 'Остаток'})
        result = result.drop(['Идентификатор_y', 'Остаток_y'], axis='columns')
        result['Идентификатор'] = result.groupby('Артикул')['Идентификатор'].ffill()
        # result = result.fillna(0)

        # print(result)

        body = {
                'data': [
                    {
                        'range': 'Склады',
                        'majorDimension': 'COLUMNS',
                        'values': [result.columns.values.tolist()]
                    },
                    {
                        'range' : 'Склады',
                        'majorDimension': 'ROWS',
                        'values': result.values.tolist()
                    }
                ]
            }

        request = my_table.spreadsheets().values().update(spreadsheetId=sheet,
                                                            valueInputOption='USER_ENTERED',
                                                            range='Склады',
                                                            body={'values': [result.columns.tolist()] + result.values.tolist()}).execute()
        print(today)
        time.sleep(2)
    time.sleep(180)
